from django.urls import path
from .views import *

urlpatterns = [
    path('', list_books, name='books_list'),
    path('<int:index>/', book_detail, name='book_detail'),
    path('author/<int:index>/', author_info, name='author_info'),
    path('book/<int:ind>/', author_books, name='author_books'),
]
